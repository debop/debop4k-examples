package debop4k.coroutines.examples.jdk8

import debop4k.coroutines.examples.AbstractCoroutinesTests
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.future.*
import org.junit.jupiter.api.Test
import java.util.concurrent.*
import java.util.concurrent.CancellationException
import kotlin.test.assertEquals

class FutureTests : AbstractCoroutinesTests() {

  @Test fun `simple await`() {
    val future = future {
      CompletableFuture.supplyAsync { "O" }.await() + "K"
    }
    assertEquals("OK", future.get())
  }

  @Test fun `completable future`() {
    val toAwait = CompletableFuture<String>()

    val future = future {
      toAwait.await() + "K"
    }
    toAwait.complete("O")

    assertEquals("OK", future.get())
  }

  @Test fun `asCompletableFuture example`() {
    val defered = async(CommonPool) {
      log { "Busy ..." }
      delay(1000)
      log { "Done!!!" }
      42
    }
    val future = defered.asCompletableFuture()
    println("Got ${future.get()}")
  }

  @Test fun `with timeout`() {
    fun slow(s: String) = future {
      delay(500L)
      s
    }

    val f = future<String> {
      log { "Started f" }
      val a = slow("A").await()
      log { "a = $a" }
      withTimeout(1000L) {
        val b = slow("B").await()
        log { "b = $b" }
      }
      try {
        withTimeout(750L) {
          val c = slow("C").await()
          log { "c = $c" }
          val d = slow("D").await()
          log { "d = $d" }
        }
      } catch (ex: CancellationException) {
        log { "timed out with $ex" }
      }
      val e = slow("E").await()
      log { "e = $e" }
      "done"
    }

    logger.debug { "f.get() = ${f.get()}" }
  }
}