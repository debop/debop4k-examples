package debop4k.coroutines.examples

import java.io.OutputStream
import java.io.PrintStream
import java.util.concurrent.*

private const val WAIT_LOST_THREADS = 10_000L // 10s
private val ignoreLostThreads = mutableSetOf<String>()

fun ignoreLostThreads(vararg s: String) {
  ignoreLostThreads += s
}

fun currentThreads(): Set<Thread> {
  var estimate = 0
  while (true) {
    estimate = estimate.coerceAtLeast(Thread.activeCount() + 1)
    val arrayOfThreads = Array<Thread?>(estimate) { null }
    val n = Thread.enumerate(arrayOfThreads)
    if (n >= estimate) {
      estimate = n + 1
      continue // retry with a better size estimate
    }
    val threads = hashSetOf<Thread>()
    for (i in 0 until n)
      threads.add(arrayOfThreads[i]!!)
    return threads
  }
}

fun checkTestThreads(threadsBefore: Set<Thread>) {
  // give threads some time to shutdown
  val waitTill = System.currentTimeMillis() + WAIT_LOST_THREADS
  var diff: List<Thread>
  do {
    val threadsAfter = currentThreads()
    diff = (threadsAfter - threadsBefore).filter { thread ->
      ignoreLostThreads.none { prefix -> thread.name.startsWith(prefix) }
    }
    if (diff.isEmpty()) break
  } while (System.currentTimeMillis() <= waitTill)
  ignoreLostThreads.clear()
  if (diff.isEmpty()) return
  val message = "Lost threads ${diff.map { it.name }}"
  println("!!! $message")
  println("=== Dumping lost thread stack traces")
  diff.forEach { thread ->
    println("Thread \"${thread.name}\" ${thread.state}")
    val trace = thread.stackTrace
    for (t in trace) println("\tat ${t.className}.${t.methodName}(${t.fileName}:${t.lineNumber})")
    println()
  }
  println("===")
  error(message)
}

// helper function to dump exception to stdout for ease of debugging failed tests
private inline fun <T> outputException(name: String, block: () -> T): T =
    try {
      block()
    } catch (e: Throwable) {
      println("--- Failed test$name")
      e.printStackTrace(System.out)
      throw e
    }

private const val SHUTDOWN_TIMEOUT = 5000L // 5 sec at most to wait


private class TeeOutput(
    private val bytesOut: OutputStream,
    private val oldOut: PrintStream
                       ) : OutputStream() {
  val limit = 200
  var lineLength = 0

  fun flushLine(): Boolean {
    if (lineLength > limit)
      oldOut.print(" ($lineLength chars in total)")
    val result = lineLength > 0
    lineLength = 0
    return result
  }

  override fun write(b: Int) {
    bytesOut.write(b)
    if (b == 0x0d || b == 0x0a) { // new line
      flushLine()
      oldOut.write(b)
    } else {
      lineLength++
      if (lineLength <= limit)
        oldOut.write(b)
    }
  }
}

private val NOT_PARKED = -1L

private class ThreadStatus {
  @Volatile @JvmField
  var parkedTill = NOT_PARKED
  @Volatile @JvmField
  var permit = false

  override fun toString(): String = "parkedTill = ${TimeUnit.NANOSECONDS.toMillis(parkedTill)} ms, permit = $permit"
}

private val MAX_WAIT_NANOS = 10_000_000_000L // 10s
private val REAL_TIME_STEP_NANOS = 200_000_000L // 200 ms
private val REAL_PARK_NANOS = 10_000_000L // 10 ms -- park for a little to better track real-time

