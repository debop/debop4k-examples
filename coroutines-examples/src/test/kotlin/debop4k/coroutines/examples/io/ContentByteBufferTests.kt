package debop4k.coroutines.examples.io

import debop4k.coroutines.examples.AbstractCoroutinesTests
import kotlinx.coroutines.experimental.io.ByteReadChannel
import kotlinx.coroutines.experimental.runBlocking
import org.junit.jupiter.api.Test
import java.nio.ByteBuffer
import java.util.*
import kotlin.test.*

class ContentByteBufferTests : AbstractCoroutinesTests() {

  @Test
  fun emptyContent() = runBlocking {

    val ch = ByteReadChannel(ByteArray(0))
    assertEquals(0, ch.availableForRead)
    assertEquals(-1, ch.readAvailable(ByteBuffer.allocate(100)))
    assertTrue { ch.isClosedForRead }
    assertTrue { ch.isClosedForWrite }
  }

  @Test
  fun singleByteContent() = runBlocking {

    val ch = ByteReadChannel(byteArrayOf(1))
    assertEquals(1, ch.availableForRead)
    assertFalse { ch.isClosedForRead }
    assertEquals(1, ch.readAvailable(ByteBuffer.allocate(100)))
    assertEquals(0, ch.availableForRead)
    assertTrue { ch.isClosedForRead }
    assertTrue { ch.isClosedForWrite }
  }

  @Test
  fun singleByteContent2() = runBlocking {

    val ch = ByteReadChannel(byteArrayOf(0x34))
    assertEquals(1, ch.availableForRead)
    assertFalse { ch.isClosedForRead }
    assertEquals(0x34, ch.readByte())
    assertEquals(0, ch.availableForRead)
    assertTrue { ch.isClosedForRead }
    assertTrue { ch.isClosedForWrite }
  }

  @Test
  fun `multiple byte content`() = runBlocking {

    val arr = ByteArray(16)
    Random().nextBytes(arr)

    val ch = ByteReadChannel(arr)
    assertEquals(16, ch.availableForRead)
    assertFalse { ch.isClosedForRead }

    ch.readByte()   // 1
    ch.readShort()  // 2
    ch.readInt()    // 4
    ch.readLong()   // 8
    ch.readByte()   // 1

    assertTrue { ch.isClosedForRead }
    assertTrue { ch.isClosedForWrite }
  }
}