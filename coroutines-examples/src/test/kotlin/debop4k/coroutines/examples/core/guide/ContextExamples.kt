package debop4k.coroutines.examples.core.guide

import debop4k.coroutines.examples.AbstractCoroutinesTests
import kotlinx.coroutines.experimental.*
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ContextExamples : AbstractCoroutinesTests() {

  @Test
  fun `thread by context`() = runBlocking<Unit> {
    val jobs = arrayListOf<Job>()

    jobs += launch(Unconfined) {
      println("      'Unconfined': I'm working in thread ${Thread.currentThread().name}")
      delay(500)
      println("      'Unconfined': After delay in thread ${Thread.currentThread().name}")
    }

    jobs += launch(coroutineContext) {
      println("'coroutineContext': I'm working in thread ${Thread.currentThread().name}")
      delay(500)
      println("'coroutineContext': After delay in thread ${Thread.currentThread().name}")
    }

    jobs += launch(CommonPool) {
      println("      'CommonPool': I'm working in thread ${Thread.currentThread().name}")
      delay(500)
      println("      'CommonPool': After delay in thread ${Thread.currentThread().name}")
    }

    jobs.forEach { it.join() }
  }

  @Test
  fun `async await`() = runBlocking<Unit> {
    val a = async(coroutineContext) {
      logger.debug { "I'm computing a piece of the answer..." }
      6
    }
    val b = async(coroutineContext) {
      logger.debug { "I'm computing another piece of the answer..." }
      7
    }

    assertEquals(13, a.await() + b.await())
    logger.debug { "The answer is ${a.getCompleted() + b.getCompleted()}" }
  }


  @Test
  fun `nested thread context`() {
    val ctx1 = newSingleThreadContext("Ctx1")
    val ctx2 = newSingleThreadContext("Ctx2")

    runBlocking(ctx1) {
      log { "Started in ctx1" }

      run(ctx2) {
        log { "Working in ctx2" }
      }
      log { "Back to ctx1" }
    }
  }

  @Test fun `cancellation affected`() = runBlocking<Unit> {

    val request = launch {

      // 독립적인 context 를 사용하므로, 상위 job cancel 시에도 계속 수행합니다.
      val job1 = launch {
        log { "job1: 자체 cotenxt 를 이용하여 독립적으로 수행합니다." }
        delay(1000)
        log { "job1: 나는 request job의 취소에 영향을 받지 않습니다." }
      }
      // 상위 Job 과 같은 coroutine context 를 사용하므로 cancel 된다. 
      val job2 = launch(coroutineContext) {
        log { "job2: request context 를 물려받았습니다." }
        delay(1000)
        log { "job2: request context 이 취소된다면, 이 로그는 찍히지 않을 것입니다." }
      }
      // 상위 Job 과 같은 coroutine context 를 사용하므로 cancel 된다.
      val job3 = launch(coroutineContext + CommonPool) {
        log { "job3: request context + CommonPool 하에서 실행합니다." }
        delay(1000)
        log { "job4: request context 이 취소된다면, 이 로그는 찍히지 않을 것입니다." }
      }
      job1.join()
      job2.join()
      job3.join()
    }

    delay(500)
    request.cancel()
    delay(1000)
    log { "main: 어떤 놈이 request 취소 후에 살아 남았나요?" }
  }

  @Test fun `nested job without join`() = runBlocking {

    val request = launch {
      repeat(3) { i ->
        launch(coroutineContext) {
          delay((i + 1) * 200L)
          println("Coroutine $i is done.")
        }
      }
      println("request: 난 작업완료. 자식 작업이 끝난는지 굳이 기다릴 필요 없음")
    }

    request.join()
    println("request 작업 완료")
  }

  @Test fun `job cancel and join`() = runBlocking {

    // lifecycle 를 관리할 job
    val job = Job()

    // 10개의 coroutine 을 생성
    val coroutines = List(10) {
      // 10 개의 job 은 위의 job 의 자식 job 이 됩니다.
      launch(coroutineContext + job) {
        delay((it + 1) * 200L)
        log { "Coroutine $it is done" }
      }
    }

    log { "Launched ${coroutines.size} coroutines" }
    delay(500L)
    log { "Job 취소 !!!" }
    job.cancelAndJoin()  // 모든 coroutines 를 취소하고, 종료되기를 기다립니다.

  }

  @Test fun `use CoroutineName`() = runBlocking(CoroutineName("main")) {
    log { "Start main coroutine ..." }

    val v1 = async(CoroutineName("v1coroutine")) {
      log { "Computing v1" }
      delay(500)
      252
    }

    val v2 = async(CoroutineName("v2coroutine")) {
      log { "Computing v2" }
      delay(1000)
      6
    }

    log { "The answer for v1 / v2 = ${v1.await() / v2.await()}" }
  }
}