package debop4k.coroutines.examples.core.guide

import debop4k.coroutines.examples.AbstractCoroutinesTests
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.*
import org.junit.jupiter.api.Test
import kotlin.coroutines.experimental.CoroutineContext

class ChannelExamples : AbstractCoroutinesTests() {

  @Test
  fun `channel send example`() = runBlocking<Unit> {
    val channel = Channel<Int>()
    launch {
      for (x in 1 .. 5) channel.send(x * x)
    }

    repeat(5) { log { "received: ${channel.receive()}" } }
    log { "Done" }
  }

  @Test
  fun `channel receive by foreach`() = runBlocking<Unit> {
    val channel = Channel<Int>()
    launch {
      for (x in 1 .. 5) channel.send(x * x)
      channel.close()
    }

    for (y in channel) log { "received: $y" }
    log { "Done" }
  }


  @Test fun `procude squares`() {

    fun produceSquares() = produce {
      for (x in 1 .. 5) send(x * x)
    }

    runBlocking<Unit> {
      val squares = produceSquares()
      squares.consumeEach { log { "consume $it" } }
      println("Done!")
    }
  }

  @Test fun `produce and receive channel`() {

    fun produceNumbers() = produce {
      var x = 1
      while (true) send(x++)
    }

    fun square(numbers: ReceiveChannel<Int>) = produce<Int> {
      for (x in numbers) send(x * x)
    }

    runBlocking<Unit> {
      val numbers = produceNumbers()
      val squares = square(numbers)

      for (i in 1 .. 5) println(squares.receive())
      println("Done!")

      // 큰 규모의 App에서는 cancel 을 호출해야 합니다.
      squares.cancel()
      numbers.cancel()
    }
  }

  @Test fun `filtering procuding numbers`() {
    fun numbersFrom(context: CoroutineContext, start: Int) = produce<Int>(context) {
      var x = start
      while (true) send(x++)
    }

    fun filter(context: CoroutineContext, numbers: ReceiveChannel<Int>, prime: Int) = produce<Int>(context) {
      for (x in numbers)
        if (x % prime != 0) send(x)
    }

    runBlocking {
      var cur: ProducerJob<Int> = numbersFrom(coroutineContext, 2)
      for (i in 1 .. 10) {
        val prime = cur.receive()
        log { "Prime=$prime" }
        cur = filter(coroutineContext, cur, prime)
      }

      // 자식 Job들을 모두 취소하여, runBlocking 을 종료합니다. 
      coroutineContext.cancelChildren()
    }
  }

  @Test fun `multiple launch`() {
    fun produceNumbers() = produce<Int> {
      var x = 1
      while (true) {
        send(x++)     // produce next
        delay(100)
      }
    }

    // 비동기로 channel로부터 정보를 받아 작업을 수행합니다.
    fun launchProcessor(id: Int, channel: ReceiveChannel<Int>) = launch {
      channel.consumeEach {
        log { "Processor #$id received $it" }
      }
    }

    runBlocking {
      val producer = produceNumbers()

      repeat(5) { launchProcessor(it, producer) }

      delay(950)
      // producing 을 취소하고, 모두 중단합니다. 
      producer.cancel()
    }
  }

  suspend fun sendString(channel: SendChannel<String>, s: String, time: Long) {
    while (true) {
      delay(time)
      channel.send(s)
    }
  }

  @Test fun `multiple sending data`() = runBlocking<Unit> {
    val channel = Channel<String>()

    launch(coroutineContext) { sendString(channel, "foo", 200L) }
    launch(coroutineContext) { sendString(channel, "BAR!", 500L) }

    repeat(6) {
      log { channel.receive() }
    }

    coroutineContext.cancelChildren()
  }

  @Test fun `buffered channel`() = runBlocking<Unit> {
    val channel = Channel<Int>(5) // buffered channel
    val sender = launch(coroutineContext) {
      repeat(10) {
        log { "Sending $it" }
        channel.send(it)
      }
    }

    // 아무것도 받지 않고 기다립니다.
    delay(1000)
    sender.cancel()
  }

  data class Ball(var hits: Int)

  suspend fun player(name: String, table: Channel<Ball>) {
    for (ball in table) {   // ball 을 수신 받습니다.
      ball.hits++
      log { "$name $ball" }
      delay(300)
      table.send(ball)  // ball 을 재전송 합니다.
    }
  }

  @Test fun `ping pong`() = runBlocking<Unit> {
    val table = Channel<Ball>()  // 공유 테이블

    launch(coroutineContext) { player("ping", table) }    // ping 이 먼저 수신, hists 를 증가시킨 후 재전송
    launch(coroutineContext) { player("pong", table) }    // pong 이 수신, hists 를 증가시킨 후 재전송  

    table.send(Ball(0))
    delay(1000)

    coroutineContext.cancelChildren() // 자식 coroutine 종료 
  }
}