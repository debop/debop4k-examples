package debop4k.coroutines.examples.nio

import debop4k.coroutines.examples.AbstractCoroutinesTests
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.nio.*
import kotlinx.coroutines.experimental.runBlocking
import org.apache.commons.io.FileUtils
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.*
import java.nio.file.StandardOpenOption
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AsyncIOTests : AbstractCoroutinesTests() {

  @get:Rule val tmpDir = TemporaryFolder()

  @Test
  fun `file channels`() {
    val inputFile: File = tmpDir.newFile()
    val outputFile: File = tmpDir.newFile()

    FileUtils.writeStringToFile(inputFile,
                                (1 .. 100_000).map(Int::toString).joinToString(""),
                                Charsets.UTF_8)

    val input = AsynchronousFileChannel.open(inputFile.toPath(), StandardOpenOption.READ)
    val output = AsynchronousFileChannel.open(outputFile.toPath(),
                                              StandardOpenOption.CREATE, StandardOpenOption.WRITE)
    val buf = ByteBuffer.allocate(4096)

    runBlocking {
      var totalBytesRead = 0L
      var totalBytesWritten = 0L

      while (totalBytesRead < input.size()) {
        while (buf.hasRemaining() && totalBytesRead < input.size()) {
          // async read
          totalBytesRead += input.aRead(buf, totalBytesRead)
        }

        buf.flip()
        logger.debug { "Read ${buf.remaining()} bytes..." }
        while (buf.hasRemaining()) {
          // async write
          totalBytesWritten += output.aWrite(buf, totalBytesWritten)
        }

        buf.clear()
      }
    }

    input.close()
    output.close()

    assertTrue { FileUtils.contentEquals(inputFile, outputFile) }
  }

  @Test fun `network channels`() = runBlocking {
    val serverChannel = AsynchronousServerSocketChannel.open().bind(InetSocketAddress(0))
    val serverPort = (serverChannel.localAddress as InetSocketAddress).port

    val c1 = launch(coroutineContext) {
      val client = serverChannel.aAccept()
      val buffer = ByteBuffer.allocate(2)
      client.aRead(buffer)
      buffer.flip()

      val msg = Charsets.UTF_8.decode(buffer).toString()
      assertEquals("OK", msg)
      logger.debug { "Server receive message=$msg" }

      client.aWrite(Charsets.UTF_8.encode("123"))
      client.close()
    }

    val c2 = launch(coroutineContext) {
      val connection = AsynchronousSocketChannel.open()

      // async call
      connection.aConnect(InetSocketAddress("127.0.0.1", serverPort))
      connection.aWrite(Charsets.UTF_8.encode("OK"))
      logger.debug { "Client send message=OK" }

      val buffer = ByteBuffer.allocate(3)

      // async call
      connection.aRead(buffer)
      buffer.flip()

      val msg = Charsets.UTF_8.decode(buffer).toString()
      assertEquals("123", msg)
      logger.debug { "Client receive string=$msg" }
    }

    c1.join()
    c2.join()
  }

}