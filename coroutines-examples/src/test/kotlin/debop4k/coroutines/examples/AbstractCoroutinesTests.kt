package debop4k.coroutines.examples

import mu.KLogging


abstract class AbstractCoroutinesTests {

  companion object : KLogging()


  protected suspend fun log(msg: suspend () -> String) = println("[${Thread.currentThread().name}] ${msg.invoke()}")

}