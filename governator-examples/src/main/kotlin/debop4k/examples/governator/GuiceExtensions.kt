package debop4k.examples.governator

import com.google.inject.AbstractModule
import com.google.inject.Binder


fun moduleOf(configurer: (Binder) -> Unit): AbstractModule {
  return object : AbstractModule() {
    override fun configure() {
      configurer.invoke(this.binder())
    }
  }
}