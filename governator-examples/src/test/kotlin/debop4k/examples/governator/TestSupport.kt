package debop4k.examples.governator

import com.google.inject.AbstractModule
import com.google.inject.Stage.PRODUCTION
import com.netflix.governator.*
import java.util.*

class TestSupport {

  class InstancesModule(vararg _instances: Any) : AbstractModule() {

    val instances = arrayListOf(*_instances)

    override fun configure() {
      instances.forEach {
        bind(it.javaClass).toInstance(it)
      }
    }
  }

  companion object {

    fun asModule(vararg instances: Any): InstancesModule = InstancesModule(instances)

    fun inject(vararg instances: Any): LifecycleInjector = InjectorBuilder.fromModule(InstancesModule(*instances)).createInjector()
  }

  private val module = InstancesModule()
  private val features = IdentityHashMap<GovernatorFeature<*>, Any?>()


  fun <T> withFeature(feature: GovernatorFeature<*>, value: T): TestSupport {
    features.put(feature, value)
    return this
  }

  fun withSingleton(vararg instances: Any): TestSupport {
    module.instances.addAll(instances)
    return this
  }

  fun inject(): LifecycleInjector {
    return LifecycleInjectorCreator()
        .withFeatures(features)
        .createInjector(PRODUCTION, module)
  }
}