package debop4k.examples.governator

import com.google.inject.CreationException
import com.google.inject.Injector
import com.netflix.governator.spi.LifecycleListener
import mu.KLogging
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.lang.AssertionError
import java.util.*
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import javax.inject.Inject

class LifecycleModuleTests {

  companion object : KLogging()

  class TestRuntimeException : RuntimeException {
    constructor() : super()
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(cause: Throwable) : super(cause)
  }

  enum class Events {
    Injected, Initialized, Destroyed, Started, Stopped, Error
  }

  open class TrackingLifecycleListener(val name: String) : LifecycleListener {
    val events = LinkedList<Events>()

    @Inject
    open fun injected(injector: Injector) {
      events.add(Events.Injected)
    }

    @PostConstruct
    open fun initialized() {
      events.add(Events.Initialized)
    }

    override fun onStarted() {
      events.add(Events.Started)
    }

    override fun onStopped(error: Throwable?) {
      events.add(Events.Stopped)
      error?.run { events.add(Events.Error) }
    }

    @PreDestroy
    open fun destroyed() {
      events.add(Events.Destroyed)
    }

    override fun toString(): String = "TrackingLifecycleListener@$name"
  }

  @DisplayName("LifecycleListener")
  @Test fun `LifecycleListener events for successful start`() {
    val listener = TrackingLifecycleListener("LifecycleListener")

    TestSupport.inject(listener).close()

    assertThat(listener.events).isEqualTo(listOf(Events.Injected,
                                                 Events.Initialized,
                                                 Events.Started,
                                                 Events.Stopped,
                                                 Events.Destroyed))
  }

  @Test fun `LifecycleListener events for RuntimeException injected`() {
    val listener = object : TrackingLifecycleListener("LifecycleListener") {
      // override 했더라도 annotation 은 지정해주어야 한다. (annotation 은 상속이 안되거든요)
      @Inject
      override fun injected(injector: Injector) {
        super.injected(injector)
        throw RuntimeException("injected failed")
      }
    }

    try {
      TestSupport.inject(listener)
      fail("expected exception injecting instance")
    } catch (e: CreationException) {
      // Nothing to do
    } catch (e: Exception) {
      fail("expected CreationException injecting instance but got $e")
    }
    assertThat(listener.events).containsOnly(Events.Injected)
  }

  @Test fun `LifecycleListener events for RuntimeException post construct`() {
    val listener = object : TrackingLifecycleListener("LifecycleListener") {
      // override 했더라도 annotation 은 지정해주어야 한다. (annotation 은 상속이 안되거든요)
      @PostConstruct
      override fun initialized() {
        super.initialized()
        throw TestRuntimeException("postconstructor runtime exception")
      }
    }

    try {
      TestSupport.inject(listener)
      fail("expected runtime exception at starting injector")
    } catch (e: CreationException) {
      // Nothing to do
    } catch (e: Exception) {
      fail("expected CreationException starting injector but got $e")
    }
    assertThat(listener.events).containsExactly(Events.Injected, Events.Initialized, Events.Stopped, Events.Error)
  }

  @Test fun `LifecycleListener events for RuntimeException on started`() {
    val listener = object : TrackingLifecycleListener("LifecycleListener") {
      // override 했더라도 annotation 은 지정해주어야 한다. (annotation 은 상속이 안되거든요)
      override fun onStarted() {
        super.onStarted()
        throw TestRuntimeException("on started runtime exception")
      }
    }

    try {
      TestSupport.inject(listener)
      fail("expected runtime exception on started")
    } catch (e: TestRuntimeException) {
      // Nothing to do
    } catch (e: Exception) {
      fail("expected CreationException on started but got $e")
    }
    assertThat(listener.events).containsExactly(Events.Injected, Events.Initialized, Events.Started, Events.Stopped, Events.Error, Events.Destroyed)
  }

  @Test fun `LifecycleListener events for RuntimeException on stopped`() {
    val listener = object : TrackingLifecycleListener("LifecycleListener") {
      override fun onStopped(error: Throwable?) {
        super.onStopped(error)
        throw TestRuntimeException("on stopped runtime exception")
      }
    }

    TestSupport.inject(listener).use { }

    assertThat(listener.events).containsExactly(Events.Injected, Events.Initialized, Events.Started, Events.Stopped, Events.Destroyed)
  }

  @Test
  fun `LifecycleListener events for RuntimeException on pre destroy`() {
    val listener = object : TrackingLifecycleListener("LifecycleListener") {
      @PreDestroy
      override fun destroyed() {
        super.destroyed()
        throw TestRuntimeException("destroyed runtime exception")
      }
    }

    TestSupport.inject(listener).use { }

    assertThat(listener.events).containsExactly(Events.Injected, Events.Initialized, Events.Started, Events.Stopped, Events.Destroyed)
  }

  @Test
  fun `when Error inject`() {
    val listener = object : TrackingLifecycleListener("LifecycleListener") {
      @Inject
      override fun injected(injector: Injector) {
        super.injected(injector)
        fail("injected exception")
      }
    }

    assertThatThrownBy {
      TestSupport.inject(listener).use { }
    }.isInstanceOf(CreationException::class.java)

    assertThatThrownBy {
      assertThat(listener.events).containsExactly(Events.Injected, Events.Initialized, Events.Stopped, Events.Error)
    }.isInstanceOf(AssertionError::class.java)
  }
}