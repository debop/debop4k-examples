package debop4k.examples.governator.package1

import com.google.inject.AbstractModule
import com.netflix.governator.annotations.AutoBindSingleton
import javax.inject.Singleton

@AutoBindSingleton(eager = false)
class AutoBindLazySingleton {

}

@AutoBindSingleton
class AutoBindModule : AbstractModule() {
  override fun configure() {
    bind(String::class.java).toInstance("AutoBound")
  }
}

interface AutoBindSingletonInterface {
}

@AutoBindSingleton
class AutoBindSingletonConcrete {
}

@AutoBindSingleton(AutoBindSingletonInterface::class)
class AutoBindSingletonWithInterface : AutoBindSingletonInterface {

}

@AutoBindSingleton(multiple = true, value = AutoBindSingletonInterface::class)
@Singleton
class AutoBindSingletonMultiBinding : AutoBindSingletonInterface {

}

