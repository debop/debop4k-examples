package debop4k.examples.governator.package1

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.netflix.governator.annotations.AutoBindSingleton
import javax.inject.Singleton


class Foo : AbstractModule() {

  @Provides
  @Singleton
  fun getFoo(): Foo = Foo()

  override fun configure() {
    // Nothing to do.
  }

}

@AutoBindSingleton
class FooModule : AbstractModule() {

  @Provides
  @Singleton
  fun getFoo(): Foo = Foo()

  override fun configure() {
    // Nothing to do.
  }

  override fun equals(other: Any?): Boolean = javaClass == other?.javaClass ?: false

  override fun hashCode(): Int = javaClass.hashCode()
}