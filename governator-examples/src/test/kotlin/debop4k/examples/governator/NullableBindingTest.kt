package debop4k.examples.governator

import com.google.inject.AbstractModule
import com.google.inject.Injector
import com.google.inject.util.Providers
import com.netflix.governator.InjectorBuilder
import com.netflix.governator.event.guava.GuavaApplicationEventModule
import org.junit.Before
import org.junit.Test
import javax.annotation.Nullable
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class NullableBindingTest {

  lateinit var injector: Injector

  @Before fun setup() {
    injector = InjectorBuilder
        .fromModules(GuavaApplicationEventModule(),
                     object : AbstractModule() {
                       override fun configure() {
                         bind(InnerDependency::class.java).toProvider(Providers.of<InnerDependency>(null))
                       }
                     })
        .createInjector()
  }

  // `@Nullable` 을 꼭 지정해줘야 null 을 지정할 수 있습니다. 아마 Guice 가 jsr-305 로 검사하나봅니다.
  private class OuterDependency @Inject constructor(@Nullable val innerDependency: InnerDependency?) {}

  @Singleton
  private class InnerDependency {}

  @Test fun `null injection`() {
    val instance = injector.getInstance(OuterDependency::class.java)
    assertNotNull(instance)
    assertNull(instance.innerDependency)
  }
}