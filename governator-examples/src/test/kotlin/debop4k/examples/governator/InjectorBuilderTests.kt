package debop4k.examples.governator

import com.google.inject.AbstractModule
import com.netflix.governator.InjectorBuilder
import com.netflix.governator.LifecycleInjectorCreator
import com.netflix.governator.visitors.*
import com.nhaarman.mockito_kotlin.*
import mu.KLogging
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.rules.TestName
import java.util.concurrent.atomic.*
import java.util.function.*
import javax.inject.Inject
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class InjectorBuilderTests {

  companion object : KLogging()

  @Test fun `lifecycle injector events`() {
    val injectCalled = AtomicBoolean(false)
    val afterInjectorCalled = AtomicBoolean(false)

    InjectorBuilder
        .fromModule(object : AbstractModule() {
          override fun configure() {
            // Nothing to do.
          }
        })
        .forEachElement(BindingTracingVisitor()) { logger.debug(it) }
        .createInjector(object : LifecycleInjectorCreator() {
          @Inject
          fun initialize() {
            injectCalled.set(true)
          }

          override fun onCompletedInjectorCreate() {
            afterInjectorCalled.set(true)
          }
        })
        .use {
          assertTrue { injectCalled.get() }
          assertTrue { afterInjectorCalled.get() }

          Assertions.assertTrue { injectCalled.get() }
          Assertions.assertTrue { afterInjectorCalled.get() }
        }
  }

  @Rule @JvmField var name = TestName()

  @BeforeEach fun printTestHeader() {
    println("==========================================")
    println("  Running Test : ${name.methodName} ")
    println("==========================================")
  }

  @Test fun `binding tracing`() {
    InjectorBuilder
        .fromModule(object : AbstractModule() {
          override fun configure() {
            bind(String::class.java).toInstance("Hello world")
          }
        })
        .forEachElement(BindingTracingVisitor()) { logger.debug(it) }
        .createInjector()
        .use {}
  }

  @Test fun `for each binding`() {
    val consumer = mock<Consumer<String>>()
    InjectorBuilder
        .fromModule { binder -> binder.bind(String::class.java).toInstance("Hello world") }
        .forEachElement(WarnOfToInstanceInjectionVisitor(), consumer)
        .createInjector()
        .use {
          verify(consumer, times(1)).accept(any<String>())
        }
  }

  @Test fun `key tracing`() {
    InjectorBuilder
        .fromModule { binder -> binder.bind(String::class.java).toInstance("Hello world") }
        .forEachElement(KeyTracingVisitor()) { logger.debug(it) }
        .createInjector()
        .use {}
  }

  @Test fun `warn on static injection`() {
    val elements = InjectorBuilder
        .fromModule { binder -> binder.requestStaticInjection(String::class.java) }
        .warnOfStaticInjections()
        .elements

    assertEquals(1, elements.size)
  }

  @Test fun `strip static injection`() {
    val elements = InjectorBuilder
        .fromModule { binder -> binder.requestStaticInjection(String::class.java) }
        .stripStaticInjections()
        .warnOfStaticInjections()
        .elements

    assertEquals(0, elements.size)
  }

  class ModuleA : AbstractModule() {
    override fun configure() {
      install(ModuleB())
      install(ModuleC())
    }
  }

  class ModuleB : AbstractModule() {
    override fun configure() {
      install(ModuleC())
    }
  }

  class ModuleC : AbstractModule() {
    override fun configure() {
      bind(String::class.java).toInstance("Hello world")
    }

    override fun equals(other: Any?): Boolean {
      return other?.javaClass?.equals(javaClass) ?: false
    }

    override fun hashCode(): Int {
      return javaClass.hashCode()
    }
  }

  @Test fun `trace modules`() {
    InjectorBuilder
        .fromModule(ModuleA())
        .forEachElement(ModuleSourceTracingVisitor()) { logger.debug(it) }
        .createInjector()
        .use { }
  }

}