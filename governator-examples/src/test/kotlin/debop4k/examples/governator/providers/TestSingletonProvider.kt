package debop4k.examples.governator.providers

import com.netflix.governator.providers.SingletonProvider
import org.junit.Test
import kotlin.test.assertEquals

class TestSingletonProvider {

  class Foo {}

  class StringProvider : SingletonProvider<Foo>() {
    override fun create(): Foo = Foo()
  }

  @Test fun `singleton behavior`() {
    val provider = StringProvider()
    val foo1 = provider.get()
    val foo2 = provider.get()

    assertEquals(foo2, foo1)
  }
}