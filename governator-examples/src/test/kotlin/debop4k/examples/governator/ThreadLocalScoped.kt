package debop4k.examples.governator

import com.google.inject.ScopeAnnotation
import kotlin.annotation.AnnotationRetention.RUNTIME

@Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@Retention(RUNTIME)
@ScopeAnnotation
annotation class ThreadLocalScoped()