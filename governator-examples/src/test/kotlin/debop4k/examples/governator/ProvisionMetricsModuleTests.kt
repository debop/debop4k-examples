package debop4k.examples.governator

import com.google.inject.Key
import com.netflix.governator.*
import com.netflix.governator.ProvisionMetrics.Element
import com.netflix.governator.ProvisionMetrics.Visitor
import com.netflix.governator.visitors.ProvisionListenerTracingVisitor
import org.junit.Test
import java.util.concurrent.*
import javax.annotation.PostConstruct
import javax.inject.Singleton
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ProvisionMetricsModuleTests {

  @Singleton
  class Foo {
    init {
      TimeUnit.MILLISECONDS.sleep(200)
    }

    @PostConstruct
    fun init() {
      TimeUnit.MILLISECONDS.sleep(200)
    }
  }

  inner class KeyTrackingVisitor(private val key: Key<*>) : Visitor {
    var element: Element? = null
      private set

    override fun visit(element: Element) {
      if (element.key == key) {
        this.element = element
      }
    }
  }

  @Test
  fun `metrics include post constructor`() {
    InjectorBuilder
        .fromModules(ProvisionMetricsModule(),
                     moduleOf { it.bind(Foo::class.java).asEagerSingleton() })
        .forEachElement(ProvisionListenerTracingVisitor())
        .createInjector()
        .use { injector ->

          val metrics = injector.getInstance(ProvisionMetrics::class.java)
          val keyTracker = KeyTrackingVisitor(Key.get(Foo::class.java))
          metrics.accept(keyTracker)

          assertNotNull(keyTracker.element)
          assertTrue { keyTracker.element?.getTotalDuration(TimeUnit.MILLISECONDS) ?: 0 > 300 }
        }
  }
}