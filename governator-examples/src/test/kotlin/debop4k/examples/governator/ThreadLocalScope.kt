package debop4k.examples.governator

import com.google.inject.*

class ThreadLocalScope : Scope {

  private val content: ThreadLocal<MutableMap<Key<*>, Any>?> = ThreadLocal<MutableMap<Key<*>, Any>?>()

  fun enter() {
    check(content.get() == null) {
      "ThreadLocalScope already exists in thread ${Thread.currentThread().id}"
      content.set(hashMapOf())
    }
  }

  fun exit() {
    check(content.get() != null) {
      "No ThreadLocalScope found in thread ${Thread.currentThread().id}"
      content.remove()
    }
  }

  override fun <T : Any?> scope(key: Key<T>, unscoped: Provider<T>): Provider<T> {
    return Provider {
      val scopedObject = content.get()
                         ?: throw OutOfScopeException("No ThreadLocalScope found in thread ${Thread.currentThread().id}")


      @Suppress("UNCHECKED_CAST")
      var current: T = scopedObject[key] as T
      if (current == null && !scopedObject.containsKey(key)) {
        current = unscoped.get()

        if (Scopes.isCircularProxy(current)) {
          return@Provider current
        }
        scopedObject.put(key, current as Any)
      }
      current
    }
  }
}