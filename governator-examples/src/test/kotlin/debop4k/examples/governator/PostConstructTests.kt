package debop4k.examples.governator

import com.google.inject.*
import com.netflix.governator.GovernatorFeatures
import com.netflix.governator.InjectorBuilder
import com.nhaarman.mockito_kotlin.*
import org.junit.*
import org.junit.rules.TestName
import org.mockito.Mockito
import javax.annotation.PostConstruct
import kotlin.test.assertNotNull

class PostConstructTests {

  private open class SimplePostConstruct {
    @PostConstruct
    open fun init() {
    }
  }

  private open class InvalidPostConstructs {
    @PostConstruct
    open fun initWithReturnValue(): String = "invalid return type"

    companion object {
      @PostConstruct
      @JvmStatic fun initStatic() {
        // can't use static method type
        throw RuntimeException("boom")
      }
    }

    @PostConstruct
    open fun initWithParameters(invalidArg: String) {
      // can't use method parameters
    }
  }

  private open class PostConstructParent1 {
    @PostConstruct
    open fun init() {
      println("parent.init")
    }
  }

  private open class PostConstructChild1 : PostConstructParent1() {
    @PostConstruct
    override fun init() {
      println("child.init")
    }
  }

  private open class PostConstructParent2 {
    @PostConstruct
    open fun anotherInit() {
      println("parent.anotherInit")
    }
  }

  private open class PostConstructChild2 : PostConstructParent2() {
    @PostConstruct
    open fun init() {
      println("child.init")
    }
  }

  private open class PostConstructParent3 {
    @PostConstruct
    open fun init() {
    }
  }

  private open class PostConstructChild3 : PostConstructParent3() {
    override fun init() {
      println("init invoked")
    }
  }

  open class MultiplePostConstructs {
    @PostConstruct
    open fun init1() {
      println("init1")
    }

    @PostConstruct
    open fun init2() {
      println("init2")
    }
  }

  @Rule @JvmField val name = TestName()

  @Before
  fun printTestHeader() {
    println("========================================================")
    println("  Running Test: ${name.methodName}")
    println("========================================================")
  }

  @Test
  fun `lifecycle init inheritance 1`() {
    val postConstructChild = mock<PostConstructChild1>()
    val inOrder = inOrder(postConstructChild)

    TestSupport.inject(postConstructChild).use { injector ->
      assertNotNull(injector.getInstance(postConstructChild.javaClass))
      // not twice
      inOrder.verify(postConstructChild, times(1)).init()
    }
  }

  @Test
  fun `lifecycle init inheritance 2`() {
    val postConstructChild = mock<PostConstructChild2>()
    val inOrder = inOrder(postConstructChild)

    TestSupport.inject(postConstructChild).use { injector ->
      assertNotNull(injector.getInstance(postConstructChild.javaClass))

      // not twice
      inOrder.verify(postConstructChild, times(1)).init()

      // NOTE: Java 에서는 parent의 method 를 먼저 호출하지만, Kotlin 에서는 child의 method를 먼저 호출한다. 
      // parent postConstruct before child postConstruct
      inOrder.verify(postConstructChild, times(1)).anotherInit()
    }
  }

  @Test
  fun `lifecycle init inheritance 3`() {
    val postConstructChild = mock<PostConstructChild3>()
    val inOrder = inOrder(postConstructChild)

    TestSupport.inject(postConstructChild).use { injector ->
      assertNotNull(injector.getInstance(postConstructChild.javaClass))
      verify(postConstructChild, never()).init()
    }
    // never, child class overrides method without annotation
    inOrder.verify(postConstructChild, never()).init()
  }

  @Test
  fun `lifecycle multiple annotation`() {
    val multiplePostConstructors = spy(MultiplePostConstructs())
    TestSupport()
        .withFeature(GovernatorFeatures.STRICT_JSR250_VALIDATION, true)
        .withSingleton(multiplePostConstructors)
        .inject()
        .use { injector ->
          assertNotNull(injector.getInstance(multiplePostConstructors.javaClass))
          verify(multiplePostConstructors, never()).init1()
          verify(multiplePostConstructors, never()).init2()
        }
    verify(multiplePostConstructors, never()).init1()
    verify(multiplePostConstructors, never()).init2()
  }

  @Test
  fun `lifecycle init with invalid post constructs`() {
    val mockInstance = mock<InvalidPostConstructs>()
    TestSupport()
        .withFeature(GovernatorFeatures.STRICT_JSR250_VALIDATION, true)
        .withSingleton(mockInstance)
        .inject()
        .use { injector ->
          assertNotNull(injector.getInstance(InvalidPostConstructs::class.java))

          verify(mockInstance, never()).initWithParameters(Mockito.anyString())
          verify(mockInstance, never()).initWithReturnValue()
        }
  }

  @Test
  fun `lifecycle init with post construct exception`() {
    val mockInstance = mock<InvalidPostConstructs>()
    TestSupport()
        .withFeature(GovernatorFeatures.STRICT_JSR250_VALIDATION, true)
        .withSingleton(mockInstance)
        .inject()
        .use { injector ->
          assertNotNull(injector.getInstance(InvalidPostConstructs::class.java))

          verify(mockInstance, never()).initWithParameters(Mockito.anyString())
          verify(mockInstance, never()).initWithReturnValue()
        }
  }

  @Test
  fun `lifecycle init`() {
    val mockInstance = mock<SimplePostConstruct>()
    TestSupport.inject(mockInstance).use { injector ->
      assertNotNull(injector.getInstance(SimplePostConstruct::class.java))
      verify(mockInstance, times(1)).init()
    }
  }

  @Test
  fun `lifecycle init with at provides`() {
    val simplePostConstruct = mock<SimplePostConstruct>()

    val builder = InjectorBuilder.fromModule(object : AbstractModule() {
      override fun configure() {}
      @Provides
      @Singleton
      fun getSimplePostConstruct(): SimplePostConstruct = simplePostConstruct
    })

    builder.createInjector().use { injector ->
      verify(injector.getInstance(SimplePostConstruct::class.java), times(1)).init()
    }
  }
}