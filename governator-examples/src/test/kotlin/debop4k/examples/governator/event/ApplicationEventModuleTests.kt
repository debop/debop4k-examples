package debop4k.examples.governator.event

import com.google.inject.AbstractModule
import com.netflix.governator.InjectorBuilder
import com.netflix.governator.LifecycleInjector
import com.netflix.governator.event.*
import com.netflix.governator.event.guava.GuavaApplicationEventModule
import org.junit.*
import java.util.concurrent.atomic.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ApplicationEventModuleTests {

  private lateinit var injector: LifecycleInjector

  @Before
  fun setup() {
    injector = InjectorBuilder
        .fromModules(GuavaApplicationEventModule(),
                     object : AbstractModule() {
                       override fun configure() {
                         bind(TestAnnotatedListener::class.java).toInstance(TestAnnotatedListener())
                         bind(TestListenerInterface::class.java).toInstance(TestListenerInterface())
                       }
                     })
        .createInjector()
  }

  @After fun cleanup() {
    injector.close()
  }

  @Test fun `provided components present`() {
    val dispatcher = injector.getInstance(ApplicationEventDispatcher::class.java)
    val listener = injector.getInstance(TestAnnotatedListener::class.java)
    val listenerInterface = injector.getInstance(TestListenerInterface::class.java)

    assertNotNull(dispatcher)
    assertNotNull(listener)
    assertNotNull(listenerInterface)
  }

  @Test fun `annotated listener`() {
    val dispatcher = injector.getInstance(ApplicationEventDispatcher::class.java)
    val listener = injector.getInstance(TestAnnotatedListener::class.java)

    assertEquals(0, listener.invocationCount.get())
    dispatcher.publishEvent(TestEvent())
    assertEquals(1, listener.invocationCount.get())
    dispatcher.publishEvent(NotTestEvent())
    assertEquals(1, listener.invocationCount.get())
  }

  @Test fun `manually registered application event listener`() {
    val dispatcher = injector.getInstance(ApplicationEventDispatcher::class.java)
    val testEventCount = AtomicInteger()
    val notTestEventCount = AtomicInteger()
    val allEventCount = AtomicInteger()

    dispatcher.registerListener(TestEvent::class.java) { testEvent ->
      testEventCount.incrementAndGet()
    }

    dispatcher.registerListener(NotTestEvent::class.java) { noTestEvent ->
      notTestEventCount.incrementAndGet()
    }

    dispatcher.registerListener(ApplicationEvent::class.java) { appEvent ->
      allEventCount.incrementAndGet()
    }

    dispatcher.publishEvent(TestEvent())

    assertEquals(1, testEventCount.get())
    assertEquals(0, notTestEventCount.get())
    assertEquals(1, allEventCount.get())

  }

  private class TestAnnotatedListener {
    val invocationCount = AtomicInteger(0)
    @EventListener
    fun doThing(event: TestEvent) {
      invocationCount.incrementAndGet()
    }
  }

  private class TestFailFastEventListener {
    @EventListener
    fun doNothing(invalidArgumentType: String) {

    }
  }

  private class TestListenerInterface : ApplicationEventListener<TestEvent> {
    val invocationCount = AtomicInteger(0)
    override fun onEvent(event: TestEvent?) {
      invocationCount.incrementAndGet()
    }
  }

  private class TestEvent : ApplicationEvent {}
  private class NotTestEvent : ApplicationEvent {}
}