package debop4k.examples.governator

import com.google.inject.AbstractModule
import com.google.inject.Key
import com.google.inject.matcher.Matchers
import com.google.inject.spi.ProvisionListener
import com.google.inject.spi.ProvisionListener.ProvisionInvocation
import com.netflix.governator.*
import debop4k.examples.governator.package1.*
import org.junit.jupiter.api.Test
import kotlin.test.*

class AutoBindSingletonTests {

  @Test fun `confirm Bindings created for auto bind singleton`() {
    val created = hashSetOf<Class<*>>()

    InjectorBuilder
        .fromModules(ScanningModuleBuilder()
                         .forPackages("debop4k.examples.governator.package1", "debop4k.examples")
                         .addScanner(AutoBindSingletonAnnotatedClassScanner())
                         .build(),
                     object : AbstractModule() {
                       override fun configure() {
                         bindListener(Matchers.any(), object : ProvisionListener {
                           override fun <T : Any?> onProvision(provision: ProvisionInvocation<T>) {
                             val type = provision.binding.key.typeLiteral.rawType
                             if (type != null && type.name.startsWith("debop4k.examples.governator.package1")) {
                               created.add(type)
                             }
                           }
                         })
                       }
                     })
        .createInjector()
        .use { injector ->

          assertTrue { created.contains(AutoBindSingletonConcrete::class.java) }

          // BUG : multiple 이면 Set<AutoBindSingletonInterface> 에 필터링되어야 하는데, 안된다. 
          // injector.getInstance(Key.get(object : TypeLiteral<Set<AutoBindSingletonInterface>>() {}))
          injector.getInstance(Key.get(AutoBindSingletonInterface::class.java))

          // assertTrue { created.contains(AutoBindSingletonMultiBinding::class.java) }
          assertTrue { created.contains(AutoBindSingletonWithInterface::class.java) }

          // by ScanningModuleBuilder...
          assertEquals("AutoBound", injector.getInstance(String::class.java))
        }
  }

  @Test fun `confirm no bindings for excluded class`() {
    InjectorBuilder
        .fromModule {
          ScanningModuleBuilder()
              .forPackages("debop4k.examples.governator.package1")
              .addScanner(AutoBindSingletonAnnotatedClassScanner())
              .excludeClasses(AutoBindSingletonConcrete::class.java)
              .build()
        }
        .createInjector()
        .use { injector ->
          assertNull(injector.getExistingBinding(Key.get(AutoBindSingletonConcrete::class.java)))
          assertNotNull(injector.getInstance(AutoBindLazySingleton::class.java))
        }
  }

  @Test fun `confirm module deduping works with scanned classes`() {
    InjectorBuilder
        .fromModules(
            ScanningModuleBuilder()
                .forPackages("debop4k.examples.governator.package1")
                .addScanner(AutoBindSingletonAnnotatedClassScanner())
                .excludeClasses(AutoBindSingletonConcrete::class.java)
                .build(),
            FooModule())
        .createInjector()
        .use { injector ->
          assertNull(injector.getExistingBinding(Key.get(AutoBindSingletonConcrete::class.java)))
          assertNotNull(injector.getInstance(Foo::class.java))
        }
  }
}