package debop4k.spark.examples

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.api.java.JavaSparkContext

/**
 * Create [JavaSparkContext]
 */
fun sparkContextOf(appName: String, master: String = "local"): JavaSparkContext {
  val conf = SparkConf().setAppName(appName).setMaster(master)
  return JavaSparkContext(SparkContext.getOrCreate(conf))
}