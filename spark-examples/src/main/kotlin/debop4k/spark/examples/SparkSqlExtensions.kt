package debop4k.spark.examples

import org.apache.spark.SparkContext
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.sql.SparkSession


@JvmOverloads
fun sparkSessionOf(appName: String, master: String = "local"): SparkSession {
  return SparkSession.builder()
      .appName(appName)
      .master(master)
      .orCreate
}

fun sparkSessionOf(sc: SparkContext): SparkSession =
    SparkSession.builder().sparkContext(sc).orCreate

fun sparkSessionOf(jsc: JavaSparkContext): SparkSession =
    sparkSessionOf(jsc.sc())
