package debop4j.spark.examples;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class JavaSparkSQLExample {

  static SparkSession spark;
  static String peopleJsonPath = "src/test/resources/people.json";

  @BeforeClass
  public static void setupClass() {
    spark = SparkSession.builder()
                        .master("local")
                        .appName("Java Spark SQL basic examples")
                        .config("spark.some.config.option", "some-value")
                        .getOrCreate();
  }

  @AfterClass
  public static void teardownClass() {
    spark.stop();
  }

  @Test
  public void runBasicDataFrameExampe() throws Exception {

    Dataset<Row> df = spark.read().json(peopleJsonPath);

    // FIXME : 왜 예제에서는 Schema parsing 이 제대로 되는데, JUnit 에서는 안되지?
    df.show();
    df.printSchema();

    // show "name" column  
    // df.select("name").show();
  }
}
