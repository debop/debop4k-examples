package debop4k.spark.examples

import com.holdenkarau.spark.testing.SharedJavaSparkContext
import mu.KLogging
import org.apache.spark.sql.SparkSession
import java.io.Serializable

abstract class AbstractKotlinSparkTests : SharedJavaSparkContext(), Serializable {

  companion object : KLogging()

  val sparkSession: SparkSession by lazy { sparkSessionOf(sc()) }

}