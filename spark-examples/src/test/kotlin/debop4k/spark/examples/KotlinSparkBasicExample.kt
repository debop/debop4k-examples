package debop4k.spark.examples

import mu.KLogging
import org.apache.spark.api.java.JavaPairRDD
import org.apache.spark.api.java.JavaRDD
import org.junit.Test
import scala.Tuple2
import java.util.*
import java.util.regex.*

class KotlinSparkBasicExample : AbstractKotlinSparkTests() {

  companion object : KLogging()

  private val SPACE: Pattern = Pattern.compile(" ")

  @Test
  fun `word count`() {

    val lines: JavaRDD<String> = sparkSession.read().textFile("datasets/smsData.txt").javaRDD()
    val words: JavaRDD<String> = lines.flatMap { line -> line.split(SPACE).iterator() }

    val ones: JavaPairRDD<String, Int> = words.mapToPair { s -> Tuple2(s, 1) }
    val counts: JavaPairRDD<String, Int> = ones.reduceByKey { acc, n -> acc + n }

    val output = counts.collectAsMap()

    output.forEach { word, cnt -> println("$word: $cnt") }
  }

  @Test
  fun `pi calculation`() {
    val random = Random(System.currentTimeMillis())

    val slices = Runtime.getRuntime().availableProcessors()
    val n = 100_000 * slices
    val list = List(n) { it }

    val dataSet = jsc().parallelize(list, slices)

    val count = dataSet
        .map {
          val x = random.nextDouble() * 2 - 1
          val y = random.nextDouble() * 2 - 1
          if (x * x + y * y <= 1) 1 else 0
        }
        .reduce { v1, v2 -> v1 + v2 }

    println("Pi is roughly : ${4.0 * count / n}")
  }
}