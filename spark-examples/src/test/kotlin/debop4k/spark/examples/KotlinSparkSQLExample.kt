package debop4k.spark.examples

import org.apache.spark.api.java.function.MapFunction
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Encoder
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.io.Serializable


class KotlinSparkSQLExample : AbstractKotlinSparkTests() {

  open class Person(var name: String? = null,
                    var age: Int? = null) : Serializable

  val peopleJsonPath = "src/test/resources/people.json"
  val peopleTextPath = "src/test/resources/people.txt"

  @Test
  fun `run basic DataFrame example`() {

    val df: Dataset<Row> = sparkSession.read().json(peopleJsonPath)

    // FIXME : 왜 예제에서는 Schema parsing 이 제대로 되는데, JUnit 에서는 안되지?
    // Display the content of the DataFrame to stdout
    df.show()

    // print the schema in a tree format
    df.printSchema()

    // Select only the "name" column
    // df.select("name").show()
  }

  @Test
  fun `run dataset creation`() {
    val person = Person("Andy", 32)

    val personEncoder: Encoder<Person> = Encoders.bean(Person::class.java)
    val javaBeanDS: Dataset<Person> = sparkSession.createDataset(listOf(person), personEncoder)

    javaBeanDS.show()
    // +---+----+
    // |age|name|
    // +---+----+
    // | 32|Andy|
    // +---+----+

    val intEncoder = Encoders.INT()
    val primitiveDS = sparkSession.createDataset(listOf(1, 2, 3), intEncoder)
    val transformedDS: Dataset<Int> = primitiveDS.map(MapFunction { value -> value + 1 }, intEncoder)

    val transformed = transformedDS.collect() as Array<Int>
    assertThat(transformed).containsExactly(2, 3, 4)

    // FIXME : 왜 예제에서는 Schema parsing 이 제대로 되는데, JUnit 에서는 안되지?
    //    val peopleDS: Dataset<Person> = sparkSession.read().json(peopleJsonPath).`as`(personEncoder)
    //    peopleDS.show()
  }

  @Test
  fun `run infer schema`() {
    val peopleRDD = sparkSession
        .read()
        .textFile(peopleTextPath)
        .javaRDD()
        .map { line ->
          val parts = line.split(",")
          Person(parts[0], parts[1].trim().toInt())
        }

    val peopleDF: Dataset<Row> = sparkSession.createDataFrame(peopleRDD, Person::class.java)
    peopleDF.createOrReplaceTempView("people")

    val teenagersDF = sparkSession.sql("SELECT name FROM people WHERE age BETWEEN 13 AND 19")

    val stringEncoder = Encoders.STRING()
    val teenagerNamesByIndexDF = teenagersDF.map(MapFunction { "Name:${it.getString(0)}" }, stringEncoder)
    teenagerNamesByIndexDF.show()
  }
}