package debop4k.junit5.examples

import org.junit.jupiter.api.*

@DisplayName("JUnit5 - Test basics")
class JUnitBasics {

  companion object {
    @BeforeAll
    @JvmStatic
    fun beforeAll() {
      println("Before all tests (once)")
    }

    @AfterAll
    @JvmStatic fun afterAll() {
      println("After all tests (once)")
    }
  }

  @BeforeEach fun beforeEach() = println("Runs before each test")
  @AfterEach fun afterEach() = println("Runs after each test")


  @Test
  fun standardTest() {
    println("Test is running")
  }

  @DisplayName("My #2 JUnit5 test")
  @Test
  fun testWithCustomDisplayName() {
    println("Test is running")
  }

  @DisplayName("Tagged JUnit5 test")
  @Tag("cool")
  @Test
  fun tagged() = println("Test is running")

}